This repository contains solutions for:
❖ COLLECTIONS TASKS
➢ https://learn.by/courses/course-v1:EPAM+JColl+ext1/courseware/2c1004eb30e64bae9b37ff47e5f23406/297eb262310a4daa9d37a6ec7825bed2/1
➢ Create a container that encapsulates an array of String, and that only adds Strings and gets Strings, so that there are no casting issues during use. If the internal array isn’t big enough for the next add, your container should automatically resize it. In main( ), compare the performance of your container with an ArrayList holding Strings.
➢ Create a class containing two String objects and make it Comparable so that the
comparison only cares about the first String. Fill an array and an ArrayList with
objects of your class by using a custom generator (eg, which generates pairs of
Country-Capital). Demonstrate that sorting works properly. Now make a
Comparator that only cares about the second String and demonstrate that
sorting works properly. Also perform a binary search using your Comparator..
❖ LOGICAL TASKS FOR ARRAYS
➢ Дано два масиви. Сформувати третій масив, що складається з тих елементів,
які: а) присутні в обох масивах; б) присутні тільки в одному з масивів.
➢ Видалити в масиві всі числа, які повторюються більше двох разів.
➢ Знайти в масиві всі серії однакових елементів, які йдуть підряд, і видалити з
них всі елементи крім одного.
➢ Герой комп'ютерної гри, що володіє силою в 25 балів, знаходиться в круглому
залі, з якого ведуть 10 закритих дверей. За кожними дверима героя чекає або
магічний артефакт, що дарує силу від 10 до 80 балів, або монстр, який має
силу від 5 до 100 балів, з яким герою потрібно битися. Битву виграє персонаж,
що володіє найбільшою силою; якщо сили рівні, перемагає герой.
✓ Організувати введення інформації про те, що знаходиться за дверима, або
заповнити її, використовуючи генератор випадкових чисел.
✓ Вивести цю саму інформацію на екран в зрозумілому табличному вигляді.
✓ Порахувати, за скількома дверима героя чекає смерть. Рекурсивно.
✓ Вивести номери дверей в тому порядку, в якому слід їх відкривати герою,
щоб залишитися в живих, якщо таке можливо.
1. Ці задачі робити в одному Maven-проекті, лише розмістити в різних пакетах,
що мають власні вхідні точки main().
2. Назва віддаленого репозиторію Java-online-task4.
3. Файли з pdf-звітами попередніх задач мають бути також прикріплені до цього
репозиторію.