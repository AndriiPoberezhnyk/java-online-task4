package com.poberezhnyk.ArrayTasks;

import java.util.Arrays;

public class ArrayTaskFunctions {

    public static int[] fillArrayWithRandomNumbers(int length) {
        int[] array = new int[length];
        for (int i = 0; i < array.length; i++) {
            array[i] = 0 + (int) (Math.random() * 10);
        }
        return array;
    }

    public static int[] fillArrayWithoutDuplicatesFromArrays(int[] array1, int[] array2) {
        int countLengthOfNewArray = 0;
        countLengthOfNewArray += countLengthIfUnique(array1, array2);
        countLengthOfNewArray += countLengthIfUnique(array2, array1);
        int[] ArrayWithoutDuplicates = fillArrayWithUniqueValues(array1, array2, countLengthOfNewArray);
        return ArrayWithoutDuplicates;
    }

    public static int[] fillArrayWithDuplicatesFromArrays(int[] array1, int[] array2) {
        int countLengthOfNewArray = 0;
        countLengthOfNewArray += countLengthIfNotUnique(array1, array2);
        countLengthOfNewArray += countLengthIfNotUnique(array2, array1);
        int[] ArrayWithDuplicates = fillArrayWithNonUniqueValues(array1, array2, countLengthOfNewArray);
        return ArrayWithDuplicates;
    }

    public static int[] findAndDeleteDuplicatesFromArray(int[] array) {
        int lengthOfArrayWithoutDuplicates = 0;
        boolean[] addedElements = new boolean[array.length];
        Arrays.fill(addedElements, true);
        for (int i = 0; i < array.length; i++) {
            for (int j = i; j < array.length; j++) {
                if (array[i] == array[j] && i != j) {
                    addedElements[j] = false;
                }
            }
        }
        for (int i = 0; i < addedElements.length; i++) {
            if (addedElements[i]) {
                lengthOfArrayWithoutDuplicates++;
            }
        }
        int[] arrayWithoutDuplicates = new int[lengthOfArrayWithoutDuplicates];
        int lastFilledElementIndex = 0;
        for (int i = 0; i < array.length; i++) {
            if (addedElements[i]) {
                arrayWithoutDuplicates[lastFilledElementIndex] = array[i];
                lastFilledElementIndex++;
            }
        }
        return arrayWithoutDuplicates;
    }

    public static int[] swapSeriesOfElementsByOneElement(int[] array) {
        int lengthOfArrayWithoutSeriesOfDuplicates = 0;
        boolean[] addedElements = new boolean[array.length];
        Arrays.fill(addedElements, true);
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] == array[j]) {
                    addedElements[j] = false;
                    continue;
                } else {
                    break;
                }
            }
        }
        for (int i = 0; i < addedElements.length; i++) {
            if (addedElements[i]) {
                lengthOfArrayWithoutSeriesOfDuplicates++;
            }
        }
        int[] arrayWithoutDuplicates = new int[lengthOfArrayWithoutSeriesOfDuplicates];
        int lastFilledElementIndex = 0;
        for (int i = 0; i < array.length; i++) {
            if (addedElements[i]) {
                arrayWithoutDuplicates[lastFilledElementIndex] = array[i];
                lastFilledElementIndex++;
            }
        }
        return arrayWithoutDuplicates;
    }

    private static int countLengthIfUnique(int[] array1, int[] array2) {
        int countLength = 0;
        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array2.length; j++) {
                if (array1[i] == array2[j]) {
                    break;
                } else if (j == array2.length - 1) {
                    countLength++;
                }
            }
        }
        return countLength;
    }

    private static int countLengthIfNotUnique(int[] array1, int[] array2) {
        int countLength = 0;
        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array2.length; j++) {
                if (array1[i] == array2[j]) {
                    countLength++;
                    break;
                }
            }
        }
        return countLength;
    }

    private static int[] fillArrayWithUniqueValues(int[] array1, int[] array2, int length) {
        int[] array3 = new int[length];
        int countIndexOfLastFilledElement = 0;
        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array2.length; j++) {
                if (array1[i] == array2[j]) {
                    break;
                } else if (j == array2.length - 1) {
                    array3[countIndexOfLastFilledElement] = array1[i];
                    countIndexOfLastFilledElement++;
                }
            }
        }
        for (int i = 0; i < array2.length; i++) {
            for (int j = 0; j < array1.length; j++) {
                if (array2[i] == array1[j]) {
                    break;
                } else if (j == array1.length - 1) {
                    array3[countIndexOfLastFilledElement] = array2[i];
                    countIndexOfLastFilledElement++;
                }
            }
        }
        return array3;
    }

    private static int[] fillArrayWithNonUniqueValues(int[] array1, int[] array2, int length) {
        int[] array3 = new int[length];
        int countIndexOfLastFilledElement = 0;
        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array2.length; j++) {
                if (array1[i] == array2[j]) {
                    array3[countIndexOfLastFilledElement] = array1[i];
                    countIndexOfLastFilledElement++;
                    break;
                }
            }
        }
        for (int i = 0; i < array2.length; i++) {
            for (int j = 0; j < array1.length; j++) {
                if (array2[i] == array1[j]) {
                    array3[countIndexOfLastFilledElement] = array2[i];
                    countIndexOfLastFilledElement++;
                    break;
                }
            }
        }
        return array3;
    }


}
