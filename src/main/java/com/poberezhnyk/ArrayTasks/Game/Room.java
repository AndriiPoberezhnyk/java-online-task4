package com.poberezhnyk.ArrayTasks.Game;

import java.util.Random;

public class Room {
    private boolean hasMonster = false;
    private boolean hasArtifact = false;
    private boolean isOpened = false;
    private int monsterPower;
    private int artifactPower;

    public Room() {
        Random random = new Random();
        if (random.nextBoolean()) {
            hasMonster = true;
            monsterPower = 5 + (int) (Math.random() * 95);
        } else {
            hasArtifact = true;
            artifactPower = 10 + (int) (Math.random() * 70);
        }
    }

    public boolean hasMonster() {
        return hasMonster;
    }

    public boolean hasArtifact() {
        return hasArtifact;
    }

    public boolean isOpened() {
        return isOpened;
    }

    public void setOpened(boolean opened) {
        isOpened = opened;
    }

    public int getMonsterPower() {
        return monsterPower;
    }

    public int getArtifactPower() {
        return artifactPower;
    }
}
