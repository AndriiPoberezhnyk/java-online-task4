package com.poberezhnyk.ArrayTasks.Game;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello stranger, what is your name?");
        Scanner scanner = new Scanner(System.in);
        Hero hero = new Hero(scanner.next());
        Room[] hallWithRooms = new Room[10];
        for (int i = 0; i < hallWithRooms.length; i++) {
            hallWithRooms[i] = new Room();
        }
        boolean[] openedRooms = new boolean[hallWithRooms.length];
        Arrays.fill(openedRooms, false);
        int door;
        while ((countOpenedDoors(hallWithRooms) < 10) && (!hero.isDead())) {
            if (countDeadlyRooms(hallWithRooms, hero)==0){
                System.out.println("There is no deadly rooms left. Gratz");
                break;
            }
            door = chooseDoor(hallWithRooms);
            openedRooms[door-1] = enterRoom(hallWithRooms[door-1], hero);
        }
        if (hero.isDead()){
            printIfDead();
            System.out.println("You would win if chose doors this way: ");
            printDoorsOrderToWin(hallWithRooms, hero.getPower(), openedRooms);
        }else{
            printIfWif(hallWithRooms);
        }
        showRoomsInfo(hallWithRooms);
    }

    public static boolean enterRoom(Room room, Hero hero) {
        printRoom(room,hero);
        if (room.hasMonster()) {
            ifMonster(room, hero);
        }else if(room.hasArtifact()){
            ifArtifact(room, hero);
        }
        if (!hero.isDead()){
            return true;
        }else{
            return false;
        }
    }

    private static void ifMonster(Room room, Hero hero) {
        System.out.println("You see a monster");
        if (room.getMonsterPower() > hero.getPower()) {
            System.out.println("You tried to fight monster, but he is way stronger than you");
            System.out.println("You're dead");
            hero.setDead(true);
        } else {
            System.out.println("You easily smashed this weak monster. Good job, there is nothing left in this room.");
            room.setOpened(true);
        }
    }

    private static void ifArtifact(Room room, Hero hero) {
        System.out.println("You see an ancient artifact inside this room");
        System.out.println("Once you picked it up your power increased by " + room.getArtifactPower());
        hero.setPower(hero.getPower() + room.getArtifactPower());
        room.setOpened(true);
    }

    public static int chooseDoor(Room[] hallWithDoors) {
        printHall(hallWithDoors);
        System.out.println("Which door do you want to open: ");
        Scanner scanner = new Scanner(System.in);
        int door = 1;
        try {
            door = scanner.nextInt();
            while (door < 1 || door > 10) {
                System.out.println("There is no door with this number, try again");
                door = scanner.nextInt();
            }
        } catch (InputMismatchException e) {
            System.out.println("Please enter number");
            door = chooseDoor(hallWithDoors);
        }
        if (hallWithDoors[door-1].isOpened()) {
            System.out.println("You have visited this room already, enter another one");
            door = chooseDoor(hallWithDoors);
        }
        return door;
    }

    public static int countOpenedDoors(Room[] hallWithDoors) {
        int openedDoors = 0;
        for (int i = 0; i < hallWithDoors.length; i++) {
            if (hallWithDoors[i].isOpened()) {
                openedDoors++;
            }
        }
        return openedDoors;
    }

    public static void printHall(Room[] hallWithDoors){
        System.out.println(" _______________________________________ ");
        System.out.println("|***************************************|");
        System.out.println("|*****************|  1|*****************|");
        if (hallWithDoors[0].isOpened()){
            System.out.println("|*****************|/  |*****************|");
        }else{
            System.out.println("|*****************|___|*****************|");
        }
        System.out.println("|******| 10|                 |  2|******|");
        if (hallWithDoors[9].isOpened()){
            System.out.print("|******|/  |                 ");
        }else{
            System.out.print("|******|___|                 ");
        }
        if (hallWithDoors[1].isOpened()){
            System.out.println("|/  |******|");
        }else{
            System.out.println("|___|******|");
        }
        System.out.println("|******                           ******|");
        System.out.println("|*|  9|              Me           |  3|*|");
        if (hallWithDoors[8].isOpened()){
            System.out.print("|*|/  |             ( •_•)        ");
        }else{
            System.out.print("|*|___|             ( •_•)        ");
        }
        if (hallWithDoors[2].isOpened()){
            System.out.println("|/  |*|");
        }else{
            System.out.println("|___|*|");
        }
        System.out.println("|******             ( ง )ง         ******|");
        System.out.println("|*|  8|             /︶\\          |  4|*|");
        if (hallWithDoors[7].isOpened()){
            System.out.print("|*|/  |                           ");
        }else{
            System.out.print("|*|___|                           ");
        }
        if (hallWithDoors[3].isOpened()){
            System.out.println("|/  |*|");
        }else{
            System.out.println("|___|*|");
        }
        System.out.println("|****** ___                   ___ ******|");
        System.out.println("|******|  7|                 |  5|******|");
        if (hallWithDoors[6].isOpened()){
            System.out.print("|******|/  |       ___       ");
        }else{
            System.out.print("|******|___|       ___       ");
        }
        if (hallWithDoors[4].isOpened()){
            System.out.println("|/  |******|");
        }else{
            System.out.println("|___|******|");
        }
        System.out.println("|*****************|  6|*****************|");
        if (hallWithDoors[5].isOpened()){
            System.out.println("|*****************|/  |*****************|");
        }else{
            System.out.println("|*****************|___|*****************|");
        }
        System.out.println("|***************************************|");
        System.out.println(" --------------------------------------- ");
    }

    public static void printRoom(Room room,Hero hero){
        if (room.hasArtifact()){
            System.out.println(" _______________________________________ ");
            System.out.println("|***************************************|");
            System.out.println("|*  ( •_•)                             *|");
            System.out.println("|*  ( ง )ง                     *        *|");
            System.out.println("|*  /︶\\                     _/\\_      *|");
            System.out.println("|*                          |    |     *|");
            System.out.println("|*_____________________________________*|");
        }else{
            System.out.println(" _______________________________________ ");
            System.out.println("|***************************************|");
            System.out.println("|*    Me                     Monster   *|");
            System.out.println("|*    "+hero.getPower()+"                          "+room.getMonsterPower()+"   *|");
            System.out.println("|*  ( •_•)                        ^ ^  *|");
            System.out.println("|*  ( ง )ง                       (•_• )*|");
            System.out.println("|*  / ︶\\                       ୧( ୧ ) *|");
            System.out.println("|*                               /︶ \\ *|");
            System.out.println("|*_____________________________________*|");
        }

    }

    public static void printIfDead(){
        System.out.println(" _______________________________________ ");
        System.out.println("|***************************************|");
        System.out.println("|*  ( X_X)                             *|");
        System.out.println("|*  ( ง )ง       *GAME OVER*            *|");
        System.out.println("|*  /︶\\                               *|");
        System.out.println("|*                                     *|");
        System.out.println("|*_____________________________________*|");
    }

    public static void printIfWif(Room[] hallWithDoors){
        System.out.println(" _______________________________________ ");
        System.out.println("|***************************************|");
        System.out.println("|*****************|  1|*****************|");
        System.out.println("|*****************|/  |*****************|");
        System.out.println("|******| 10|                 |  2|******|");
        System.out.println("|******|/  |                 |/  |******|");
        System.out.println("|******                           ******|");
        System.out.println("|*|  9|          *YOU WON*        |  3|*|");
        System.out.println("|*|/  |             ( ^_^)        |/  |*|");
        System.out.println("|******             ( ง )ง         ******|");
        System.out.println("|*|  8|             /︶\\          |  4|*|");
        System.out.println("|*|/  |                           |/  |*|");
        System.out.println("|****** ___                   ___ ******|");
        System.out.println("|******|  7|                 |  5|******|");
        System.out.println("|******|/  |       ___       |/  |******|");
        System.out.println("|*****************|  6|*****************|");
        System.out.println("|*****************|/  |*****************|");
        System.out.println("|***************************************|");
        System.out.println("|_______________________________________|");
    }

    public static void showRoomsInfo(Room[] hallWithRooms){
        System.out.println();
        System.out.println("ROOMS INFORMATION");
        for (int i = 0; i < hallWithRooms.length; i++) {
            if (hallWithRooms[i].hasArtifact()){
                System.out.printf("%-7s%-9s%s%n", "Room "+(i+1)," Artifact,", "power = " + hallWithRooms[i].getArtifactPower());
            }else if(hallWithRooms[i].hasMonster()){
                System.out.printf("%-7s%-9s%s%n", "Room "+(i+1)," Monster,", "power = " + hallWithRooms[i].getMonsterPower());
            }
        }
    }

    public static int countDeadlyRooms(Room[] hallWithRooms, Hero hero){
        int deadlyRooms=0;
        for (int i = 0; i < hallWithRooms.length; i++) {
            if (hallWithRooms[i].hasMonster()){
                if (hallWithRooms[i].getMonsterPower()>hero.getPower()){
                    deadlyRooms++;
                }
            }
        }
        return deadlyRooms;
    }

    public static void printDoorsOrderToWin(Room[] hallWithRooms,int heroPower, boolean[] openedRooms){
        System.out.print("Doors order: ");
        for (int i = 0; i < hallWithRooms.length; i++) {
            if (openedRooms[i] == false) {
                if (hallWithRooms[i].hasArtifact()) {
                    heroPower += hallWithRooms[i].getArtifactPower();
                    System.out.print((i+1) + ", ");
                    openedRooms[i] = true;
                }
                if (hallWithRooms[i].hasMonster()) {
                    if (heroPower >= hallWithRooms[i].getMonsterPower()) {
                        System.out.print((i+1) + ", ");
                        openedRooms[i] = true;
                    }
                }

            }
        }
        for (int i = 0; i < openedRooms.length; i++) {
            if (openedRooms[i]==false){
                if (heroPower>=hallWithRooms[i].getMonsterPower()){
                    System.out.print((i+1) +", ");
                    openedRooms[i] = true;
                }else{
                    System.out.println(" No, you wouldn't");
                    break;
                }
            }
        }
    }
}
