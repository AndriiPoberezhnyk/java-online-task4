package com.poberezhnyk.ArrayTasks.Game;

public class Hero {
    private String name;
    private int power = 25;
    private boolean isDead = false;

    public Hero(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public boolean isDead() {
        return isDead;
    }

    public void setDead(boolean dead) {
        isDead = dead;
    }
}
