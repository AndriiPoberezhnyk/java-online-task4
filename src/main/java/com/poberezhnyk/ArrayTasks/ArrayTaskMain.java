package com.poberezhnyk.ArrayTasks;

import java.util.Arrays;
import java.util.Scanner;

public class ArrayTaskMain {

    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        System.out.println("Enter number of elements for 1st array: ");
        int n = scanner.nextInt();
        int[] array1 = ArrayTaskFunctions.fillArrayWithRandomNumbers(n);
        System.out.println("Enter number of elements for 2nd array: ");
        n = scanner.nextInt();
        int[] array2 = ArrayTaskFunctions.fillArrayWithRandomNumbers(n);
        System.out.println("First array: " + Arrays.toString(array1));
        System.out.println("Second array: " + Arrays.toString(array2));
        System.out.print("Array with unique values from both arrays: ");
        int[] arrayWithoutDuplicates = ArrayTaskFunctions.fillArrayWithoutDuplicatesFromArrays(array1,array2);
        System.out.println(Arrays.toString(arrayWithoutDuplicates));
        System.out.print("Array with non-unique values from both arrays: ");
        int[] arrayWithDuplicates = ArrayTaskFunctions.fillArrayWithDuplicatesFromArrays(array1,array2);
        System.out.println(Arrays.toString(arrayWithDuplicates));
        System.out.println("Array1: " + Arrays.toString(array1));
        System.out.print("Array1 without duplicates: ");
        array1 = ArrayTaskFunctions.findAndDeleteDuplicatesFromArray(array1);
        System.out.println(Arrays.toString(array1));
        System.out.println("Array2: " + Arrays.toString(array2));
        System.out.print("Array2 without series of the same elements: ");
        array2 = ArrayTaskFunctions.swapSeriesOfElementsByOneElement(array2);
        System.out.println(Arrays.toString(array2));
    }
}
