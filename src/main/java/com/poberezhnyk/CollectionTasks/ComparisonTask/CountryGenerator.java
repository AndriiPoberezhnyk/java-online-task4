package com.poberezhnyk.CollectionTasks.ComparisonTask;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class CountryGenerator {
    private List<Country> countries;
    private Random rnd = new Random();

    public CountryGenerator() {
        countries = new ArrayList<>();
        init();
    }

    public void init(){
        StringBuilder sb = new StringBuilder();
        try (FileReader reader = new FileReader("countries.txt");
             BufferedReader br = new BufferedReader(reader)) {
            sb.append(br.readLine());
            while(!sb.toString().trim().equals("null")) {
                countries.add(new Country(sb.substring(0, sb.indexOf("&")).trim(), sb.substring(sb.indexOf("&")+1)));
                sb = new StringBuilder();
                sb.append(br.readLine());
            }
        } catch (IOException e) {
            System.err.format("IOException: %s%n", e);
        }
    }

    public Country[] getArrayWithCountries(int size){
        Country[] countryArray = new Country[size];
        try{
            Set<Country> countrySet = generateRandomCountrySetBySize(size);
            countryArray = countrySet.toArray(new Country[size]);
        } catch (IndexOutOfBoundsException e){
            System.out.println("Wrong size");
        }
        return countryArray;
    }
    public ArrayList<Country> getArrayListWithCountries(int size){
        ArrayList<Country> countryArrayList = new ArrayList<>();
        try{
            Set<Country> countrySet = generateRandomCountrySetBySize(size);
            countryArrayList.addAll(countrySet);
        } catch (IndexOutOfBoundsException e){
            System.out.println("Wrong size");
        }
        return countryArrayList;
    }

    private Set<Country> generateRandomCountrySetBySize(int size){
        Set<Country> countriesArray = new HashSet<>();
        while(countriesArray.size() != size){
            Country randomCountry = countries.get(0 + (int)(Math.random()*countries.size()));
            countriesArray.add(randomCountry);
        }
        return countriesArray;
    }

    public List<Country> getCountries() {
        return countries;
    }
}
