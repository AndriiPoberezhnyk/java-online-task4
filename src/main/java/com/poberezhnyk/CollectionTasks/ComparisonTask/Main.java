package com.poberezhnyk.CollectionTasks.ComparisonTask;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        Menu menu = new Menu();
        menu.createArrays();
        menu.initUserInterface();
    }
}
