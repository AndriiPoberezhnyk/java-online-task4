package com.poberezhnyk.CollectionTasks.ComparisonTask;

import java.util.Comparator;

public class CapitalComparator implements Comparator<Country> {
    @Override
    public int compare(Country o1, Country o2) {
        return o1.getCapital().compareTo(o2.getCapital());
    }
}
