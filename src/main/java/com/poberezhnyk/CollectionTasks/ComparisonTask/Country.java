package com.poberezhnyk.CollectionTasks.ComparisonTask;

public class Country implements Comparable<Country>{
    private String name;
    private String capital;

    public Country(String countryName, String countryCapital) {
        this.name = countryName;
        this.capital = countryCapital;
    }

    public String getName() {
        return name;
    }

    public String getCapital() {
        return capital;
    }

    @Override
    public int compareTo(Country o) {
        return name.compareTo(o.getName());
    }
}
