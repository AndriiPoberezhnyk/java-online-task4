package com.poberezhnyk.CollectionTasks.ComparisonTask;

import java.util.*;

public class Menu {
    private Scanner scanner = new Scanner(System.in);
    private CountryGenerator countryGenerator;
    Country[] countriesArray;
    ArrayList<Country> countriesArrayList;

    public Menu() {
        countryGenerator = new CountryGenerator();
    }

    public void printMainMenu() {
        System.out.println("Main menu: ");
        System.out.println("1. Show arrays");
        System.out.println("2: Sort arrays by default and print");
        System.out.println("3: Sort arrays by capital and print");
        System.out.println("4: Search country position");
        System.out.println("5: Exit");
    }

    public int createArrays() {
        System.out.println("Let's start by creating arrays");
        System.out.println("Enter size of the array: ");
        int size = 10;
        try {
            size = scanner.nextInt();
            while (size < 0 || size > countryGenerator.getCountries().size()) {
                System.out.println("Wrong size, try again");
            }
        } catch (InputMismatchException e) {
            scanner.reset();
            scanner.next();
            size = createArrays();
        }
        countriesArray = countryGenerator.getArrayWithCountries(size);
        countriesArrayList = countryGenerator.getArrayListWithCountries(size);
        return size;
    }

    public int initUserInterface() {
        int userChoice = 0;
        while (userChoice != 5) {
            printMainMenu();
            try {
                userChoice = scanner.nextInt();
                while (userChoice < 0 || userChoice > 5) {
                    System.out.println("Wrong choice, try again");
                    userChoice = scanner.nextInt();
                }
            } catch (InputMismatchException e) {
                System.out.println("Wrong input type, try again");
                scanner.reset();
                scanner.next();
                userChoice = initUserInterface();
                return userChoice;
            }
            showInfoByUserChoice(userChoice);
        }
        return userChoice;
    }

    public void showInfoByUserChoice(int userChoice) {
        switch (userChoice) {
            case 1: {
                System.out.println("Country[] elements: ");
                printArray(countriesArray);
                System.out.println("ArrayList<Country> elements: ");
                printArrayList(countriesArrayList);
            }
            break;
            case 2: {
                Arrays.sort(countriesArray);
                System.out.println("Sorted by default Country[] elements: ");
                printArray(countriesArray);
                Collections.sort(countriesArrayList);
                System.out.println("Sorted by default ArrayList<Country> elements: ");
                printArrayList(countriesArrayList);
            }
            break;
            case 3: {
                Arrays.sort(countriesArray, new CapitalComparator());
                System.out.println("Sorted by capital Country[] elements: ");
                printArray(countriesArray);
                Collections.sort(countriesArrayList, new CapitalComparator());
                System.out.println("Sorted by capital ArrayList<Country> elements: ");
                printArrayList(countriesArrayList);
            }
            break;
            case 4: {
                System.out.print("Please enter country name: ");
                String countryName = scanner.nextLine();
                scanner.nextLine();
                System.out.print("Now enter country capital: ");
                String countryCapital = scanner.nextLine();
                Country userCountry = new Country(countryName, countryCapital);
                int countryListIndex = Collections.binarySearch(countriesArrayList,
                        userCountry, new CapitalComparator());
                int countryArrayIndex = Arrays.binarySearch(countriesArray, userCountry, new CapitalComparator());
                System.out.println("Country[] elements: ");
                printArray(countriesArray);
                if (countryArrayIndex <0){
                    System.out.println("There is no such country in array");
                } else{
                    System.out.println("Country position = "+ countryArrayIndex);
                }
                System.out.println("ArrayList<Country> elements: ");
                printArrayList(countriesArrayList);
                if (countryListIndex <0){
                    System.out.println("There is no such country in array list");
                } else{
                    System.out.println("Country position = "+ countryListIndex);
                }
            }
            break;
            case 5:{
                System.out.println("Goodbye.");
            }break;
        }
    }

    private void printArray(Country[] countriesArray) {
        System.out.println("*----------------------Start Country[]---------------------*");
        for (int i = 1; i <= countriesArray.length; i++) {
            System.out.println(i + ". Country = " + countriesArray[i - 1].getName()
                    + ", Capital = " + countriesArray[i - 1].getCapital());
        }
        System.out.println("*----------------------End Country[]-----------------------*");
    }

    private void printArrayList(ArrayList<Country> countriesArrayList) {
        System.out.println("*----------------------Start ArrayList<Country>------------*");
        for (int i = 1; i <= countriesArrayList.size(); i++) {
            System.out.println(i + ". Country = " + countriesArrayList.get(i - 1).getName()
                    + ", Capital = " + countriesArrayList.get(i - 1).getCapital());
        }
        System.out.println("*----------------------End ArrayList<Country>--------------*");
    }
}
