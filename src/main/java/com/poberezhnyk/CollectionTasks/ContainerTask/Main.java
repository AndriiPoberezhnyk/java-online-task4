package com.poberezhnyk.CollectionTasks.ContainerTask;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<String> myArrayList = new ArrayList<>(10);
        String[] arr = new String[11];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = Integer.toString(i);
            myArrayList.add(Integer.toString(i));
        }
        StringContainer myContainer = new StringContainer(arr);
        for (int i = 0; i < myContainer.getSize(); i++) {
            System.out.println("i = " + myContainer.get(i));
        }
        System.out.println();
        myArrayList.forEach(str -> System.out.println("i = " + str));
        myContainer.remove("1");
        myArrayList.remove("1");
        myContainer.add("11");
        myArrayList.add("11");
        myContainer.add("AddByIndex", 1);
        myArrayList.add(1, "AddByIndex");
        myContainer.remove(10);
        myArrayList.remove(10);
        System.out.println();
        for (int i = 0; i < myContainer.getSize(); i++) {
            System.out.println("i = " + myContainer.get(i));
        }
        System.out.println();
        myArrayList.forEach(str -> System.out.println("i = " + str));
    }
}
