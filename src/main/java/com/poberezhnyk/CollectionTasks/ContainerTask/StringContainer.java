package com.poberezhnyk.CollectionTasks.ContainerTask;

import java.util.Arrays;

public class StringContainer {
    private static final int CAPACITY = 10;
    private int size;
    private String[] container;

    public StringContainer() {
        container = new String[CAPACITY];
        size = 0;
    }

    public StringContainer(int capacity) {
        if (capacity > 0) {
            container = new String[capacity];
        }
        size = 0;
    }

    public StringContainer(String[] stringArray) {
        int newCapacity = CAPACITY;
        while (newCapacity <= stringArray.length) {
            newCapacity += 6;
        }
        size = stringArray.length;
        container = Arrays.copyOf(stringArray, newCapacity);
    }

    public void add(String string) {
        if (size == container.length) {
            grow();
        }
        container[size] = string;
        size++;
    }

    public void add(String string, int index) throws IndexOutOfBoundsException {
        if (size == container.length) {
            grow();
        } else if (index > size || index < 0) {
            System.err.println("Index out of range");
            throw new IndexOutOfBoundsException();
        }
        for (int i = size; i >= index; i--) {
            container[i] = container[i - 1];
        }
        container[index] = string;
        size++;
    }

    public void remove(int index) throws IndexOutOfBoundsException {
        if (index > size || index < 0) {
            throw new IndexOutOfBoundsException();
        }
        for (int i = index; i < size; i++) {
            container[i] = container[i + 1];
        }
        container[size] = null;
        size--;
    }

    public void remove(String string) throws IndexOutOfBoundsException {
        int index = -1;
        for (int i = 0; i < size; i++) {
            if (container[i].equals(string)) {
                index = i;
                break;
            }
        }
        if (index == -1) {
            throw new IndexOutOfBoundsException();
        } else {
            remove(index);
        }
    }

    public String get(int index) throws IndexOutOfBoundsException {
        if (index >= size || index < 0) {
            throw new IndexOutOfBoundsException();
        }
        return container[index];
    }

    private void grow() {
        String[] newContainer = Arrays.copyOf(container, (container.length + 6));
        container = newContainer;
    }

    public int getSize() {
        return size;
    }

    public int capacity() {
        return container.length;
    }
}
